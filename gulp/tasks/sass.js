const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');
const browserSync = require('browser-sync');
const autoprefixer = require('gulp-autoprefixer')

gulp.task('sass',function(){
    return gulp.src('app/assets/styles/main.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({ errLogToConsole: true }))
    .pipe(autoprefixer({
        overrideBrowserslist: ['> 1%', 'last 2 versions', 'firefox >= 4', 'safari 7', 'safari 8', 'IE 8', 'IE 9', 'IE 10', 'IE 11'],
        cascade: false
    }))
    .pipe(gulp.dest('web/resources/styles'))
    .pipe(browserSync.reload({
        stream: true
    }));
});