(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

require("./modules/events");

require("./modules/video");

window.addEventListener('load', function (event) {});

},{"./modules/events":2,"./modules/video":5}],2:[function(require,module,exports){
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _functions = require("./functions");

var _slides = _interopRequireDefault(require("./slides"));

window.addEventListener('load', function (event) {
  var scrollBtn = document.getElementsByClassName("scroll");
  var frameChances = document.getElementById("frameChances");
  var slideItems = document.getElementsByClassName('slide-item');
  var slideNo = document.getElementsByClassName('frame--number');
  var btnLeft = document.getElementById("btnLeft");
  var btnRight = document.getElementById("btnRight");
  var sections = document.getElementsByClassName("section-scroll");
  var links = document.getElementsByClassName("scroll-nav");
  var questions = document.getElementsByClassName("faq__item");
  var hoverItems = document.getElementsByClassName("item--hover");
  var sectionChances = document.getElementById("chances"); // const sectionRescue =  document.getElementById("rescue");

  var userAgent = navigator.userAgent.toLowerCase();
  var menuBtn = document.getElementsByClassName("menu-btn");
  var menuNav = document.getElementById("menuNav");
  var body = document.getElementsByTagName("body");
  var closeMenu = document.getElementById("closeMenu"); // const headerSec = document.getElementById("headerSec");

  var videoPlay = document.getElementById("videoPlay");
  var videoWrapper = document.getElementsByClassName("main-video-wrapper")[0];
  var mainVideo = document.getElementById("mainVideo");
  var closeVideo = document.getElementById("closeVideo");
  var isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
  var slides = new _slides.default(slideItems.length);

  for (var i = 0; i < scrollBtn.length; i++) {
    scrollBtn[i].addEventListener('click', function (e) {
      e.preventDefault();
      var id = e.currentTarget.getAttribute("href").replace(/#/, '');
      var section = document.getElementById(id);
      body[0].style.overflow = '';
      menuNav.classList.remove("menu-visible");
      (0, _functions.scrollView)(section);
    });
  }

  for (var _i = 0; _i < hoverItems.length; _i++) {
    hoverItems[_i].addEventListener('mouseenter', function (e) {
      var startX = e.clientX;
      var startY = e.clientY;

      var _loop = function _loop(j) {
        hoverItems[j].addEventListener('mousemove', function (e) {
          var disX = (startX - e.clientX) * 0.025;
          var disY = (startY - e.clientY) * 0.025;

          if (disX > 70) {
            disX = 70;
          } else if (disX < -70) {
            disX = -70;
          }

          if (disY > 70) {
            disY = 70;
          } else if (disY < -70) {
            disY = -70;
          }

          hoverItems[j].style.transform = "translate(".concat(disX, "px, ").concat(disY, "px)");
          hoverItems[j].style.zIndex = 11;
          hoverItems[j].style.transition = 'none';
        });
      };

      for (var j = 0; j < hoverItems.length; j++) {
        _loop(j);
      }
    });
  }

  var _loop2 = function _loop2(_i2) {
    hoverItems[_i2].addEventListener('mouseleave', function () {
      hoverItems[_i2].style.transform = '';
      hoverItems[_i2].style.zIndex = null;
      hoverItems[_i2].style.transition = '';
    });
  };

  for (var _i2 = 0; _i2 < hoverItems.length; _i2++) {
    _loop2(_i2);
  }

  btnLeft.addEventListener('click', function (e) {
    slides.decrement();
    var index = slides.index;

    for (var _i3 = 0; _i3 < slideItems.length; _i3++) {
      slideItems[_i3].classList.add('hide');

      slideNo[_i3].classList.add('hidden');
    }

    slideItems[index].classList.remove('hide');
    slideNo[index].classList.remove('hidden');
    btnRight.classList.remove('not-allowed');

    if (index <= 0) {
      btnLeft.classList.add('not-allowed');
    } else {
      btnLeft.classList.remove('not-allowed');
    }
  });
  btnRight.addEventListener('click', function (e) {
    slides.increment();
    var index = slides.index;

    for (var _i4 = 0; _i4 < slideItems.length; _i4++) {
      slideItems[_i4].classList.add('hide');

      slideNo[_i4].classList.add('hidden');
    }

    slideItems[index].classList.remove('hide');
    slideNo[index].classList.remove('hidden');
    btnLeft.classList.remove('not-allowed');

    if (index >= slideItems.length - 1) {
      btnRight.classList.add('not-allowed');
    } else {
      btnRight.classList.remove('not-allowed');
    }
  });
  videoPlay.addEventListener('click', function () {
    videoWrapper.classList.add("main-video-wrapper--playing");
    mainVideo.play();
  });
  closeVideo.addEventListener('click', function () {
    mainVideo.pause();
    mainVideo.currentTime = 0;
    videoWrapper.classList.remove("main-video-wrapper--playing");
  });

  for (var _i5 = 0; _i5 < sections.length; _i5++) {
    links[_i5].classList.remove("active");

    if ((0, _functions.inView)(sections[_i5], window.innerHeight)) {
      links[_i5].classList.add("active");
    }
  }

  window.addEventListener('scroll', function (e) {
    for (var _i6 = 0; _i6 < sections.length; _i6++) {
      links[_i6].classList.remove("active");

      if ((0, _functions.inView)(sections[_i6], window.innerHeight)) {
        links[_i6].classList.add("active");
      }
    }

    if (sectionChances.getBoundingClientRect().top <= 0.015 * window.innerHeight && sectionChances.getBoundingClientRect().top >= -0.7 * window.innerHeight) {
      if (!frameChances.classList.contains('visible')) {
        var ly_timeout = setTimeout(function () {
          e.preventDefault();
          frameChances.classList.add('visible');
        }, 800);
      }
    }

    ; // if (sectionRescue.getBoundingClientRect().top <= 0* window.innerHeight) {
    //   headerSec.style.position = 'fixed';
    //   headerSec.classList.add("fixed-header");
    // } else {
    //   headerSec.style.position = '';
    //   headerSec.classList.remove("fixed-header");
    // }
  });

  var _loop3 = function _loop3(_i7) {
    questions[_i7].addEventListener('click', function (e) {
      if (e.currentTarget.classList.contains("answer--visible")) {
        e.currentTarget.classList.remove("answer--visible");
        e.currentTarget.classList.remove("arrow-up");
      } else {
        for (var j = 0; j < questions.length; j++) {
          questions[j].classList.remove("answer--visible");
          questions[j].classList.remove("arrow-up");
        }

        questions[_i7].classList.add("answer--visible");

        questions[_i7].classList.add("arrow-up");
      }
    });
  };

  for (var _i7 = 0; _i7 < questions.length; _i7++) {
    _loop3(_i7);
  }

  for (var _i8 = 0; _i8 < menuBtn.length; _i8++) {
    menuBtn[_i8].addEventListener('click', function (e) {
      body[0].style.overflow = 'hidden';
      menuNav.classList.add("menu-visible");
    });
  }

  closeMenu.addEventListener('click', function () {
    body[0].style.overflow = '';
    menuNav.classList.remove("menu-visible");
  });
});

},{"./functions":3,"./slides":4,"@babel/runtime/helpers/interopRequireDefault":8}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.inView = exports.scrollView = void 0;

var scrollView = function scrollView(element) {
  var i = element.offsetTop - element.getBoundingClientRect().top;

  if (i <= element.offsetTop) {
    window.scrollTo(0, i);
    var int = setInterval(function () {
      window.scrollTo(0, i);
      i += 5;

      if (i >= element.offsetTop) {
        clearInterval(int);
        window.scrollTo(0, element.offsetTop);
      }

      ;
    }, 5);
  } else {
    window.scrollTo(0, i);

    var _int = setInterval(function () {
      window.scrollTo(0, i);
      i -= 5;

      if (i <= element.offsetTop) {
        clearInterval(_int);
        window.scrollTo(0, element.offsetTop);
      }

      ;
    }, 5);
  }
};

exports.scrollView = scrollView;

var inView = function inView(element, windowHeight) {
  return element.getBoundingClientRect().top <= 0.5 * windowHeight && element.getBoundingClientRect().top >= -0.7 * windowHeight;
};

exports.inView = inView;

},{}],4:[function(require,module,exports){
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Slides =
/*#__PURE__*/
function () {
  function Slides(no_of_elements) {
    (0, _classCallCheck2.default)(this, Slides);
    this.index = 0;
    this.no_of_elements = no_of_elements;
  }

  (0, _createClass2.default)(Slides, [{
    key: "increment",
    value: function increment() {
      this.index++;

      if (this.index >= this.no_of_elements - 1) {
        this.index = this.no_of_elements - 1;
      }
    }
  }, {
    key: "decrement",
    value: function decrement() {
      this.index--;

      if (this.index <= 0) {
        this.index = 0;
      }
    }
  }]);
  return Slides;
}();

var _default = Slides;
exports.default = _default;

},{"@babel/runtime/helpers/classCallCheck":6,"@babel/runtime/helpers/createClass":7,"@babel/runtime/helpers/interopRequireDefault":8}],5:[function(require,module,exports){
"use strict";

window.addEventListener('load', function (event) {
  var video = document.getElementById("introVideo");

  if (!video.paused) {
    video.play();
  }
});

},{}],6:[function(require,module,exports){
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;
},{}],7:[function(require,module,exports){
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;
},{}],8:[function(require,module,exports){
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

module.exports = _interopRequireDefault;
},{}]},{},[1]);
