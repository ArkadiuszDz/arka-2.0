import { scrollView, inView } from './functions';
import Slides from './slides';

window.addEventListener('load', (event) => {

  const scrollBtn = document.getElementsByClassName("scroll");
  const frameChances = document.getElementById("frameChances");
  const slideItems = document.getElementsByClassName('slide-item');
  const slideNo = document.getElementsByClassName('frame--number');
  const btnLeft = document.getElementById("btnLeft");
  const btnRight = document.getElementById("btnRight");
  const sections = document.getElementsByClassName("section-scroll");
  const links = document.getElementsByClassName("scroll-nav");
  const questions = document.getElementsByClassName("faq__item");
  const hoverItems = document.getElementsByClassName("item--hover");
  const sectionChances =  document.getElementById("chances");
  // const sectionRescue =  document.getElementById("rescue");
  const userAgent = navigator.userAgent.toLowerCase();
  const menuBtn = document.getElementsByClassName("menu-btn");
  const menuNav = document.getElementById("menuNav");
  const body = document.getElementsByTagName("body");
  const closeMenu = document.getElementById("closeMenu");
  // const headerSec = document.getElementById("headerSec");
  const videoPlay = document.getElementById("videoPlay");
  const videoWrapper = document.getElementsByClassName("main-video-wrapper")[0];
  const mainVideo = document.getElementById("mainVideo");
  const closeVideo = document.getElementById("closeVideo");
  const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
 
  const slides = new Slides(slideItems.length);

  for (let i=0; i<scrollBtn.length; i++) {
    scrollBtn[i].addEventListener('click', (e) => {
      e.preventDefault();
      let id = e.currentTarget.getAttribute("href").replace(/#/,'');
      let section = document.getElementById(id);
      body[0].style.overflow = '';
      menuNav.classList.remove("menu-visible");
      scrollView(section);
    });
  }

  for (let i=0; i<hoverItems.length; i++) {
    hoverItems[i].addEventListener('mouseenter', (e) => {
        let startX = e.clientX;
        let startY = e.clientY;
      for (let j=0; j<hoverItems.length; j++) {
        hoverItems[j].addEventListener('mousemove', (e) => {
          let disX = (startX - e.clientX) * 0.025;
          let disY = (startY - e.clientY) * 0.025;

          if (disX > 70) {
            disX = 70;
          } else if (disX < -70) {
            disX = -70;
          }

          if (disY > 70) {
            disY = 70;
          } else if (disY < -70) {
            disY = -70;
          }
          hoverItems[j].style.transform = `translate(${disX}px, ${disY}px)`;
          hoverItems[j].style.zIndex = 11;
          hoverItems[j].style.transition = 'none';
        });
      }

    });
  }

  for(let i=0; i<hoverItems.length; i++) {
    hoverItems[i].addEventListener('mouseleave', () => {
      hoverItems[i].style.transform = '';
      hoverItems[i].style.zIndex = null;
      hoverItems[i].style.transition = '';
    });
  }
  
  btnLeft.addEventListener('click', (e) => {
    slides.decrement();
    let index = slides.index;
    for (let i=0; i< slideItems.length; i++) {
      slideItems[i].classList.add('hide');
      slideNo[i].classList.add('hidden');
    }
    slideItems[index].classList.remove('hide');
    slideNo[index].classList.remove('hidden');

    btnRight.classList.remove('not-allowed');

    if (index <= 0) {
      btnLeft.classList.add('not-allowed');
    } else {
      btnLeft.classList.remove('not-allowed');
    }
  });

  btnRight.addEventListener('click', (e) => {
    slides.increment();
    let index = slides.index;
    for (let i=0; i<slideItems.length; i++) {
      slideItems[i].classList.add('hide');
      slideNo[i].classList.add('hidden');
    }
    slideItems[index].classList.remove('hide');
    slideNo[index].classList.remove('hidden');

    btnLeft.classList.remove('not-allowed');

    if (index >= slideItems.length - 1) {
      btnRight.classList.add('not-allowed');
    } else {
      btnRight.classList.remove('not-allowed');
    }

  });

  videoPlay.addEventListener('click', () => {
    videoWrapper.classList.add("main-video-wrapper--playing");
    mainVideo.play();
  });

  closeVideo.addEventListener('click', () => {
    mainVideo.pause();
    mainVideo.currentTime = 0;
    videoWrapper.classList.remove("main-video-wrapper--playing");
  });

  for (let i=0; i<sections.length; i++) {
    links[i].classList.remove("active");
    if(inView(sections[i], window.innerHeight,)) {
      links[i].classList.add("active");
    }
  }

  window.addEventListener('scroll', (e) => {
    for (let i=0; i<sections.length; i++) {
      links[i].classList.remove("active");
      if(inView(sections[i], window.innerHeight)) {
        links[i].classList.add("active");
      }
    }
    if(sectionChances.getBoundingClientRect().top <= 0.015* window.innerHeight && sectionChances.getBoundingClientRect().top >= -0.7* window.innerHeight) {
      if (!frameChances.classList.contains('visible')) {
        let ly_timeout = setTimeout(() => {
          e.preventDefault();
          frameChances.classList.add('visible');
        }, 800);
      }
    };

    // if (sectionRescue.getBoundingClientRect().top <= 0* window.innerHeight) {
    //   headerSec.style.position = 'fixed';
    //   headerSec.classList.add("fixed-header");
    // } else {
    //   headerSec.style.position = '';
    //   headerSec.classList.remove("fixed-header");
    // }

  });

  for (let i=0; i<questions.length; i++) {
    questions[i].addEventListener('click', (e) => {
      if (e.currentTarget.classList.contains("answer--visible")) {
        e.currentTarget.classList.remove("answer--visible");
        e.currentTarget.classList.remove("arrow-up");
      } else {
        for (let j=0; j<questions.length; j++) {
          questions[j].classList.remove("answer--visible");
          questions[j].classList.remove("arrow-up");
        }
        questions[i].classList.add("answer--visible");
        questions[i].classList.add("arrow-up");
      }
      
    });
  }

  for (let i=0; i<menuBtn.length; i++) {
    menuBtn[i].addEventListener('click', (e) => {
      body[0].style.overflow = 'hidden';
      menuNav.classList.add("menu-visible");
    });
  }
 
  closeMenu.addEventListener('click', () => {
    body[0].style.overflow = '';
    menuNav.classList.remove("menu-visible");
  });

});
