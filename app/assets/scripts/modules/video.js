window.addEventListener('load', (event) => {

  const video = document.getElementById("introVideo");
  
  if (!video.paused) {
    video.play();
  }

});