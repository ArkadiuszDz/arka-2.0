const scrollView = (element) => {
  let i = element.offsetTop - element.getBoundingClientRect().top;
  if (i <= element.offsetTop) {
    window.scrollTo(0, i);
    let int = setInterval(function() {
      window.scrollTo(0, i);
      i += 5;
      if (i >= element.offsetTop) {
        clearInterval(int);
        window.scrollTo(0, element.offsetTop);
      };
    }, 5);
  } else {
    window.scrollTo(0, i);
    let int = setInterval(function() {
      window.scrollTo(0, i);
      i -= 5;
      if (i <= element.offsetTop) {
        clearInterval(int); 
        window.scrollTo(0, element.offsetTop);
      };
    }, 5);
  }
  
}

const inView = (element, windowHeight) => {
  return element.getBoundingClientRect().top <= 0.5* windowHeight && element.getBoundingClientRect().top >= -0.7* windowHeight;
}



export {scrollView, inView};