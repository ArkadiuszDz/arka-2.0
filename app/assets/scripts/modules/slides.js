class Slides {
  constructor(no_of_elements) {
    this.index = 0;
    this.no_of_elements = no_of_elements;
  }
  increment () {
    this.index++;
    if (this.index >= this.no_of_elements - 1) {
      this.index = this.no_of_elements - 1;
    }
  }
  decrement () {
    this.index--;
    if (this.index <= 0) {
      this.index = 0;
    }
  }
}

export default Slides;